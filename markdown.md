# Learning Markdown Language

# This is heading One
This is heading one
===================
## This is heading Two
this is heading two
--
### This is heading three
#### This is heading four
##### This is heading five
###### This is heading six


***

This is simple paragraph. no need to type any charecter to asign it.
What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has.

***

<p align="center">
This is text align center. You can put the value to right or lef as well.What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?
</p>

<br> this is to break the line.

***  
this is used to make a horizontal line

**This is bold text**<br>
__This is bold text__<br>
This is __bold__ text<br>
This is bo**ld** text<br>

*This is italic*<br>
_This is italic_<br>

***This is bold italic text***

***

if i put 4 space or one tab btn it will displayed like box

    What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?

***

>This is blockquote
>> This is nested blockquote
>>> This is third label blockquote

>>coming back to one step up

>This is original.

~~This is strick through line~~
<br>

***

1. list style order list<br>
   2. list style one<br>
   4. list style two
3. list item two
<br>

* list style unorder list<br>
  * nested list style
  * nested list style
      * nested list style
  *nested list style
* list style
<br><br>

- unorder list another example
  - unorder list
  - unorder list
- unorder list

***

Markdown Links: [Google](https://gooogle.com "search engine")

Link Address: <https://google.com><br>
Email: <sharifwds@gmail.com>

link on a paragraph. [Sharifwds][1] is a personal website.
here [sharifwds][2] is a portfolio site.

link with id<br>
* [table](#table)

[1]: <https://sharifwds.me> "personal Website"
[2]: <https://sharifwds.me> "portfolio Website"

***

<br>
image with alt text<br>

![coding](1.jpg 'this is coding image')


image in a link/ clickable
[![coding](2.jpg 'this is coding image')](https://sharifwds.me)

icon image with align
<p align="center">
<img src="icon.png">
</p>

Create table 

First Heading | Second Heading | Third Heading
--------------| ---------------| -------------
first Item | Second Item | Third item
Another Item | Second Item | Third item
Another Item | Second Item | Third item

***

show inline text 

This is our `<img>` tag.
```
<body>
    <h1>This is a heading</h1>
</body>
```

GitHub Flavoured syntext Hilight

html
```html
<body>
    <h1>This is a heading</h1>
    <p>This is paragraph</p>
</body>
```
Javascript
```javascript
var a = 20;
var b = 20;
console.log(a + b);
```
PHP
```php
$a = 20;
$b = 20;
echo a + b;
```
***

<br>
- [ ] checkbox<br>
- [x] checked <br>
- [ ] unchecked<br>

***
<a name="table"></a>
<details>
  <summary>This is Collapsable Block </summary>

  # Text
  This is my Markdown is a way to style text on the web. You control the display of the document; formatting words as bold or italic, adding images, and creating lists are just a few of the things we can do with Markdown. Mostly, Markdown is just regular text with a few non-alphabetic characters thrown in, like or.
</details>

<details>
  <summary>This is Collapsable Block </summary>

  # Text
  This is my Markdown is a way to style text on the web. You control the display of the document; formatting words as bold or italic, adding images, and creating lists are just a few of the things we can do with Markdown. Mostly, Markdown is just regular text with a few non-alphabetic characters thrown in, like or.
</details>

___
## Imoji <br>
:grinning:  :kissing:<br>
:star_struck: :heart_eyes:

## Keyboard

To Copy:  <kbd>CTRL</kbd> + <kbd>C</kbd> <br>
To Paste:  <kbd>CTRL</kbd> + <kbd>V</kbd>

___

## Create a badge 

Goto the website https://shields.io. <br>
create a badge
copy the url on top of the page<br>
take a img tag put the copied url into src
like 

<img src="https://img.shields.io/badge/Sharifwds-website-orange">

